import * as cdk from 'aws-cdk-lib';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class HelloCdkStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create a new S3 bucket with versioning and encryption
    new s3.Bucket(this, 'HelloCdkStack', {  // Changed the name to avoid using placeholder XXX
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED
    });
  }
}