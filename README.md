In mini-project 3, I created a S3 bucket using AWS CDK.

## Requirements
- [x] Create S3 bucket using AWS CDK with generated S3 bucket screenshot
- [x] Use CodeWhisperer to generate CDK code and writeup explaining its usage
- [x] Add bucket properties e.g. verisioning and encryption
- [x] Documentation

## Steps 
(Reference: https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html)

IAM policy required to be added: AdministratorAccess\

1. Install Node and AWS CDK
```
brew install node
npm install -g aws-cdk
```

2. Initiate a new Typescript project
```
cdk init app --language=typescript
```

3. Install necessary packages
```
npm install aws-cdk-lib
npm install typescript @types/node @types/jest --save-dev
```

4. Modify files
- Go to the ts file under the bin directory and uncomment the line 14
- Go to the ts file under the lib directory and add code

5. Deploy
```
cdk bootstrap  # Only needed once per AWS account/region
cdk deploy
```

## Screenshots
1. Generated Code
![GeneratedCode](screenshots/GeneratedCode.png)

2. CodeWhisperer in use
![CodeWhisperer](screenshots/CodeWhisperer.png)

3. Successful Bootstrap
![Bootstrap](screenshots/Bootstrap.png)

4. Successful Deployment
![Deployment](screenshots/Deploy.png)

5. Created S3 bucket with encryption and versioning properties
![Encryption](screenshots/Encryption.png)
![Versioning](screenshots/Versioning.png)

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npx cdk deploy`  deploy this stack to your default AWS account/region
* `npx cdk diff`    compare deployed stack with current state
* `npx cdk synth`   emits the synthesized CloudFormation template
